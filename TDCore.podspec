Pod::Spec.new do |s|
    s.name         = "TDCore"
    s.version      = "1.0.0"
    s.summary      = "Core shared components for iOS projects"
    s.homepage     = "https://bitbucket.org/tdesert/tdcore"

    s.license = { :type => 'MIT', :text => <<-LICENSE
        Copyright 2015 Thomas Désert. All rights reserved.
        LICENSE
    }

    s.author       = { "Thomas Désert" => "thomas.desert@hotmail.com" }

    s.platform     = :ios
    s.ios.deployment_target = '5.0'

    s.source       = { :git => "https://tdesert@bitbucket.org/tdesert/tdcore.git", :tag => "1.0.0" }

    s.source_files  = 'TDCore/**/*.{h,m}'
    # s.exclude_files = 'Classes/Exclude'
    # s.public_header_files = 'Classes/**/*.h'

    # ――― Project Linking ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
    #
    #  Link your library with frameworks, or libraries. Libraries do not include
    #  the lib prefix of their name.
    #

    # s.framework  = 'SomeFramework'
    # s.frameworks = 'SomeFramework', 'AnotherFramework'

    # s.library   = 'iconv'
    # s.libraries = 'iconv', 'xml2'

    # ――― Project Settings ――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
    #
    #  If your library depends on compiler flags you can set them in the xcconfig hash
    #  where they will only apply to your library. If you depend on other Podspecs
    #  you can include multiple dependencies to ensure it works.

    s.requires_arc = true

    # s.xcconfig = { 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2' }
    # s.dependency 'JSONKit', '~> 1.4'
end