//
//  NSArray+Helpers.h
//  TDCore
//
//  Created by Tom on 6/15/15.
//  Copyright (c) 2015 Thomas Desert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Helpers)

- (NSArray *)map:(id(^)(id obj))block;
- (NSArray *)select:(BOOL(^)(id obj))block;
- (NSArray *)arrayByRemovingObject:(id)object;
- (NSArray *)reversedArray;

@end
