//
//  NSArray+Helpers.m
//  TDCore
//
//  Created by Tom on 6/15/15.
//  Copyright (c) 2015 Thomas Desert. All rights reserved.
//

#import "NSArray+Helpers.h"

@implementation NSArray (Helpers)

- (NSArray *)map:(id (^)(id))block
{
    NSMutableArray *new = [NSMutableArray array];
    for(id obj in self)
    {
        id newObj = block(obj);
        [new addObject: newObj ? newObj : [NSNull null]];
    }
    return [NSArray arrayWithArray:new];
}

- (NSArray *)select:(BOOL (^)(id))block
{
    NSMutableArray *new = [NSMutableArray array];
    for(id obj in self)
    {
        if(block(obj))
        {
            [new addObject: obj];
        }
    }
    return [NSArray arrayWithArray:new];
}

- (NSArray *)arrayByRemovingObject:(id)object
{
    NSMutableArray *mutableCopy = [self mutableCopy];
    [mutableCopy removeObject:object];
    return [NSArray arrayWithArray:mutableCopy];
}

- (NSArray *)reversedArray
{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[self count]];
    NSEnumerator *enumerator = [self reverseObjectEnumerator];
    for (id element in enumerator) {
        [array addObject:element];
    }
    return array;
}

@end
