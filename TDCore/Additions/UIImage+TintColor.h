//
//  UIImage+TintColor.h
//  TDCore
//
//  Created by Tom on 6/15/15.
//  Copyright (c) 2015 Thomas Desert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (TintColor)

+ (UIImage *)imageNamed:(NSString *)imageName tintColor:(UIColor *)tintColor;
- (UIImage *)tintedImageWithColor:(UIColor *)tintColor;

@end
