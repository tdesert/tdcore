//
//  UIView+Constraints.h
//  TDCore
//
//  Created by Tom on 6/15/15.
//  Copyright (c) 2015 Thomas Desert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Constraints)

- (void)addConstraintsWithVisualFormat:(NSString *)visualFormat views:(NSDictionary *)views;
- (void)addConstraintsWithVisualFormat:(NSString *)visualFormat options:(NSLayoutFormatOptions)options metrics:(NSDictionary *)metrics views:(NSDictionary *)views;

@end
