//
//  UIView+Constraints.m
//  TDCore
//
//  Created by Tom on 6/15/15.
//  Copyright (c) 2015 Thomas Desert. All rights reserved.
//

#import "UIView+Constraints.h"

@implementation UIView (Constraints)

- (void)addConstraintsWithVisualFormat:(NSString *)visualFormat views:(NSDictionary *)views
{
    [self addConstraintsWithVisualFormat:visualFormat options:0 metrics:nil views:views];
}

- (void)addConstraintsWithVisualFormat:(NSString *)visualFormat options:(NSLayoutFormatOptions)options metrics:(NSDictionary *)metrics views:(NSDictionary *)views
{
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:visualFormat
                                                                   options:options
                                                                   metrics:metrics
                                                                     views:views];
    [self addConstraints:constraints];
}

@end
