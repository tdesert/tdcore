//
//  TDEventEmitter.h
//  TDCore
//
//  Created by Tom on 6/15/15.
//  Copyright (c) 2015 Thomas Desert. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^TDEventEmitterCallback)(id obj);

@interface TDEventEmitter : NSObject

- (void)on:(NSString *)event callback:(TDEventEmitterCallback)callback;
- (void)once:(NSString *)event callback:(TDEventEmitterCallback)callback;

- (void)removeCallback:(TDEventEmitterCallback)callback;
- (void)removeCallback:(TDEventEmitterCallback)callback forEvent:(NSString *)event;
- (void)removeAllCallbacksForEvent:(NSString *) event;
- (void)removeAllCallbacks;

- (void)emit:(NSString *)event object:(id)object;

@end
