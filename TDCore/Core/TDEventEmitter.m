//
//  TDEventEmitter.m
//  TDCore
//
//  Created by Tom on 6/15/15.
//  Copyright (c) 2015 Thomas Desert. All rights reserved.
//

#import "TDEventEmitter.h"

/**
 * TDEventListener
 **/

@interface TDEventListener : NSObject
@property (assign, nonatomic) BOOL once;
@property (copy, nonatomic) TDEventEmitterCallback callback;
@end

@implementation TDEventListener
@end


/**
 * TDEventEmitter
 **/

@interface TDEventEmitter ()
@property (strong, nonatomic) NSMutableDictionary *listenersMap;
@end

@implementation TDEventEmitter

- (NSMutableDictionary *)listenersMap
{
    if (!_listenersMap)
    {
        _listenersMap = [[NSMutableDictionary alloc] init];
    }
    return _listenersMap;
}

#pragma mark - Listeners registration

- (void)on:(NSString *)event callback:(TDEventEmitterCallback)callback
{
    [self addCallback:callback forEvent:event once:NO];
}

- (void)once:(NSString *)event callback:(TDEventEmitterCallback)callback
{
    [self addCallback:callback forEvent:event once:YES];
}

- (void)addCallback:(TDEventEmitterCallback)callback forEvent:(NSString *)event once:(BOOL)once
{
    NSMutableArray *listeners = [self.listenersMap objectForKey:event];
    if (!listeners)
    {
        listeners = [[NSMutableArray alloc] init];
        [self.listenersMap setValue:listeners forKey:event];
    }
    
    TDEventListener *listener = [[TDEventListener alloc] init];
    listener.once = once;
    listener.callback = callback;
    [listeners addObject:listener];
}

#pragma mark - Emitting events

- (void)emit:(NSString *)event object:(id)object
{
    NSMutableArray *listeners = [self.listenersMap objectForKey:event];
    for (TDEventListener *listener in listeners)
    {
        listener.callback(object);
    }
}

#pragma mark - Removing listeners

- (void)removeCallback:(TDEventEmitterCallback)callback
{
    for (NSString *event in self.listenersMap)
    {
        [self removeCallback:callback forEvent:event];
    }
}

- (void)removeAllCallbacks
{
    self.listenersMap = nil;
}

- (void)removeAllCallbacksForEvent:(NSString *)event
{
    [self.listenersMap removeObjectForKey:event];
}

- (void)removeCallback:(TDEventEmitterCallback)callback forEvent:(NSString *)event
{
    if (!self.listenersMap.count || !callback || !event) return;
    
    NSMutableArray *listeners = self.listenersMap[event];
    NSMutableIndexSet *discardedItems = [NSMutableIndexSet indexSet];
    [listeners enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        TDEventListener *listener = (TDEventListener *)obj;
        if (listener.callback == callback)
        {
            [discardedItems addIndex:idx];
        }
    }];
    [listeners removeObjectsAtIndexes:discardedItems];
}

@end
