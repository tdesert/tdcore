//
//  TDLogger.h
//  TDCore
//
//  Created by Tom on 6/15/15.
//  Copyright (c) 2015 Thomas Desert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TDEventEmitter.h"

// Use these macros for logging
#define TDLogDebug(format, ...)         __TDLog(TDLogLevelDebug,    TDLogFlagDebug,     format, ##__VA_ARGS__)
#define TDLogInfo(format, ...)          __TDLog(TDLogLevelInfo,     TDLogFlagInfo,      format, ##__VA_ARGS__)
#define TDLogWarning(format, ...)       __TDLog(TDLogLevelWarning,  TDLogFlagWarning,   format, ##__VA_ARGS__)
#define TDLogError(format, ...)         __TDLog(TDLogLevelError,    TDLogFlagError,     format, ##__VA_ARGS__)
#define TDLogMethod()                   TDLogDebug(@"%s", __PRETTY_FUNCTION__)

// Use these macros to annotate code with TODO/FIXME
#define TODO(__message__) __PRAGMA_MESSAGE__(message("TODO - "__message__))
#define FIXME(__message__) __PRAGMA_MESSAGE__(message("FIXME - "__message__))

#ifdef DEBUG
#define TDLogFlagDebug      @"🔹"
#define TDLogFlagInfo       @"🔸"
#define TDLogFlagWarning    @"🔶"
#define TDLogFlagError      @"🔴"
#else
#define TDLogFlagDebug      @" <debug> "
#define TDLogFlagInfo       @" <info> "
#define TDLogFlagWarning    @" <warning> "
#define TDLogFlagError      @" <error> "
#endif

#define __PRAGMA_MESSAGE__(x) _Pragma(#x)
#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

#define __TDLog(lvl, f, fmt, ...) [[TDLogger sharedLogger] log:lvl flag:f file:__FILENAME__ line:__LINE__ format:fmt, ##__VA_ARGS__]

typedef NS_ENUM(NSUInteger, TDLogLevel) {
    TDLogLevelDebug,
    TDLogLevelInfo,
    TDLogLevelWarning,
    TDLogLevelError
};

@interface TDLogger : TDEventEmitter

@property (assign, nonatomic) TDLogLevel minLogLevel;

+ (instancetype)sharedLogger;

- (void)log:(TDLogLevel)level flag:(NSString *)flag file:(const char *)file line:(int)line format:(NSString *)format, ...;

@end
