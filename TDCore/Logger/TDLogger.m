//
//  TDLogger.m
//  TDCore
//
//  Created by Tom on 6/15/15.
//  Copyright (c) 2015 Thomas Desert. All rights reserved.
//

#import "TDLogger.h"

@interface TDLogger ()
@end

@implementation TDLogger

+ (instancetype)sharedLogger
{
    static TDLogger *logger = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        logger = [[TDLogger alloc] init];
    });
    return logger;
}

- (id)init
{
    self = [super init];
    if (self)
    {
#ifdef DEBUG
        self.minLogLevel = TDLogLevelDebug;
#else
        self.minLogLevel = TDLogLevelInfo;
#endif
    }
    return self;
}

- (void)log:(TDLogLevel)level flag:(NSString *)flag file:(const char *)file line:(int)line format:(NSString *)format, ...
{
    if (level < self.minLogLevel) return;
    
    va_list ap;
    va_start(ap, format);
    NSString *content = [[NSString alloc] initWithFormat:format arguments:ap];
    va_end(ap);
    
    NSLog(@"%@(%s:%d) %@", flag, file, line, content);
    [self emit:@"log" object:[NSString stringWithFormat:@"%@%@", flag, content]];
}

@end
