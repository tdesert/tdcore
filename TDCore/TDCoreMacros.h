//
//  TDCoreMacros.h
//  TDCore
//
//  Created by Tom on 6/15/15.
//  Copyright (c) 2015 Thomas Desert. All rights reserved.
//

#ifndef TDCore_TDCoreMacros_h
#define TDCore_TDCoreMacros_h

/***
 * Helper for simulating abstract methods in objective-c.
 * 
 * Use this macro to force subclasses of an abstract class to override a method.
 * At runtime, if the abstract method is called on parent abstract class, an NSInternalInconsistencyException will be raised.
 *
 * Usage:
 *  
 * @implementation SomeAbstractClass
 *  
 *  - (void)someAbstractMethod {
 *      mustOverride();
 *  }
 *
 *  @end
 */
#define mustOverride() [NSException raise:NSInternalInconsistencyException format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]

/***
 * Performs an unknown selector on a given object.
 * These macros create a block (anonymous function) that returns the return value of the performed selector.
 *
 * Use this macro to avoid clang leak warnings when using -[NSObject performSelector:...] with non-static selectors.
 *  ie. selectors created from strings.
 * This problem, linked to ARC, is very well explained here: http://stackoverflow.com/questions/7017281/performselector-may-cause-a-leak-because-its-selector-is-unknown
 * Basically, ARC needs to know the return type of a performed selector so it can handle memory management correctly.
 * (ARC doesn't behave the same way if a method returns a retainable object, or a primitive type such as int or void).
 *
 * A less safe alternative to this macro is to disable clang warnings for unsafe selector calls :
 *
 *  #pragma clang diagnostic push
 *  #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
 *  [object performSelector:unknownSelector withObject:argument];
 *  #pragma clang diagnostic pop
 *
 *
 * Usage:
 *  NSString *myString = TDPerformSafeSelector(NSString*, myObject, NSSelectorFromString(selectorName))
 */
#define TDPerformSafeSelector(returnType, object, selector) (^{ \
    IMP imp = [object methodForSelector:selector]; \
    returnType (*func)(id, SEL) = (void *)imp; \
    return func(object, selector); \
})()

#define TDPerformSafeSelectorWithObject(returnType, object, selector, argumentType, argument) (^{ \
    IMP imp = [object methodForSelector:selector]; \
    returnType (*func)(id, SEL, argumentType) = (void *)imp; \
    return func(object, selector, argument); \
})()


/*
 *  System Versioning Preprocessor Macros
 *  (from http://stackoverflow.com/questions/3339722/check-iphone-ios-version )
 */

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

/*
 * TDString: helper for [NSString stringWithFormat...]
 */
#define TDString(fmt, ...)      [NSString stringWithFormat:fmt, ##__VA_ARGS__]

#endif
